package com.example.anthonycuttivet.TD4

/**
 * Created by anthonycuttivet on 04/12/2017.
 */
object ScoreTable {
    val NAME = "Score"
    val ID = "_id"
    val PSEUDO = "pseudo"
    val SCORE = "score"
}
