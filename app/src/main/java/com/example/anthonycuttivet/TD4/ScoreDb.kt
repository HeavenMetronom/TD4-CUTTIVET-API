package com.example.anthonycuttivet.TD4
import org.jetbrains.anko.db.*

/**
 * Created by anthonycuttivet on 04/12/2017.
 */
class ScoreDb(private val scoreDbHelper: ScoreDbHelper = ScoreDbHelper.instance) {
    fun requestScores() = scoreDbHelper.use {
        select(ScoreTable.NAME,
                ScoreTable.PSEUDO, ScoreTable.SCORE)
                .parseList(classParser<Score>())
    }

    fun saveScore(pseudo: String, score: Int) = scoreDbHelper.use {
        insert(ScoreTable.NAME,
                ScoreTable.PSEUDO to pseudo,
                ScoreTable.SCORE to score)
    }

    fun saveScores(scoreList: List<Score>) {
        for (s in scoreList)
            saveScore(s.pseudo, s.score)
    }

}