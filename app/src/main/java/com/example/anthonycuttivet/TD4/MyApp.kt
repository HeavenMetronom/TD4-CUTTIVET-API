package com.example.anthonycuttivet.TD4

import android.app.Application

/**
 * Created by anthonycuttivet on 04/12/2017.
 */
class MyApp : Application() {

    companion object {
        lateinit var instance: MyApp
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

}

